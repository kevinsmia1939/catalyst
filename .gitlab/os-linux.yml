# Linux-specific builder configurations and build commands

## Base images

.fedora36:
    image: "kitware/paraview:ci-catalyst-fedora36-20230710"
    variables:
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci

.fedora_mpich_addon:
    variables:
        MODULES: mpi/mpich-x86_64
        # Even with SHM size of 512m, SIGBUS still happened. Let's just use the
        # network instead for reliability.
        # https://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions#Q:_Why_MPI_Put_raises_SIGBUS_error_inside_docker.3F
        MPIR_CVAR_NOLOCAL: 1

.fedora36_extdeps_addon:
    variables:
        CMAKE_PREFIX_PATH: "/opt/conduit/basic"

.fedora36_extdeps_wrappings_addon:
    variables:
        CMAKE_PREFIX_PATH: "/opt/conduit/wrapped"

.fedora36_basic:
    extends:
        - .fedora36

    variables:
        CMAKE_CONFIGURATION: fedora36

.fedora36_replay:
    extends:
        - .fedora36

    variables:
        CMAKE_CONFIGURATION: fedora36_replay

.fedora36_mpi_replay:
    extends:
        - .fedora36
        - .fedora_mpich_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_mpi_replay

.fedora36_extdeps_replay:
    extends:
        - .fedora36
        - .fedora36_extdeps_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_extdeps_replay

.fedora36_extdeps_mpi_replay:
    extends:
        - .fedora36
        - .fedora36_extdeps_addon
        - .fedora_mpich_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_extdeps_mpi_replay

.fedora36_tidy:
    extends:
        - .fedora36

    variables:
        CMAKE_CONFIGURATION: fedora36_tidy
        CTEST_NO_WARNINGS_ALLOWED: 1

.fedora36_mpi_replay_tidy:
    extends:
        - .fedora36
        - .fedora_mpich_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_mpi_replay_tidy
        CTEST_NO_WARNINGS_ALLOWED: 1

.fedora36_extdeps_mpi_replay_tidy:
    extends:
        - .fedora36
        - .fedora36_extdeps_addon
        - .fedora_mpich_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_extdeps_mpi_replay_tidy
        CTEST_NO_WARNINGS_ALLOWED: 1

.fedora36_memcheck:
    extends: .fedora36

    variables:
        CMAKE_BUILD_TYPE: RelWithDebInfo

.fedora_asan_addon:
    variables:
        CTEST_MEMORYCHECK_TYPE: AddressSanitizer
        # Disable LeakSanitizer for now. It's catching all kinds of errors that
        # need investigated or suppressed.
        CTEST_MEMORYCHECK_SANITIZER_OPTIONS: detect_leaks=0

.fedora_ubsan_addon:
    variables:
        CTEST_MEMORYCHECK_TYPE: UndefinedBehaviorSanitizer

.fedora36_asan:
    extends:
        - .fedora36_memcheck
        - .fedora_asan_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_asan

.fedora36_ubsan:
    extends:
        - .fedora36_memcheck
        - .fedora_ubsan_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_ubsan

.fedora36_asan_mpi_replay:
    extends:
        - .fedora36_memcheck
        - .fedora_asan_addon
        - .fedora_mpich_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_asan_mpi_replay

.fedora36_mpi_replay_ubsan:
    extends:
        - .fedora36_memcheck
        - .fedora_ubsan_addon
        - .fedora_mpich_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_mpi_replay_ubsan

.fedora36_wrappings:
    extends:
        - .fedora36

    variables:
        CMAKE_CONFIGURATION: fedora36_wrappings

.fedora36_replay_wrappings:
    extends:
        - .fedora36

    variables:
        CMAKE_CONFIGURATION: fedora36_replay_wrappings

.fedora36_mpi_replay_wrappings:
    extends:
        - .fedora36
        - .fedora_mpich_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_mpi_replay_wrappings

.fedora36_extdeps_replay_wrappings:
    extends:
        - .fedora36
        - .fedora36_extdeps_wrappings_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_extdeps_replay_wrappings

.fedora36_extdeps_mpi_replay_wrappings:
    extends:
        - .fedora36
        - .fedora36_extdeps_wrappings_addon
        - .fedora_mpich_addon

    variables:
        CMAKE_CONFIGURATION: fedora36_extdeps_mpi_replay_wrappings

## Tags

.linux_builder_tags:
    tags:
        - build
        - paraview
        - docker
        - linux-x86_64

.linux_sanitizer_tags:
    tags:
        - privileged
        - paraview
        - docker
        - linux-x86_64

## Linux-specific scripts

.before_script_linux: &before_script_linux
    - .gitlab/ci/cmake.sh
    - .gitlab/ci/ninja.sh
    - export PATH=$PWD/.gitlab:$PWD/.gitlab/cmake/bin:$PATH
    - cmake --version
    - ninja --version
    - "$LAUNCHER .gitlab/ci/gtest.sh"
    # Load any modules that may be necessary.
    - '[ -n "$MODULES" ] && . /etc/profile.d/modules.sh && module load $MODULES'

.cmake_build_linux:
    stage: build-test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest -VV -S .gitlab/ci/ctest_configure.cmake"
        - "$LAUNCHER ctest -VV -S .gitlab/ci/ctest_build.cmake"

    interruptible: true
    timeout: 5 minutes

.cmake_build_test_linux:
    stage: build-test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest -VV -S .gitlab/ci/ctest_configure.cmake"
        - "$LAUNCHER ctest -VV -S .gitlab/ci/ctest_build.cmake"
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_test.cmake"

    interruptible: true
    timeout: 5 minutes

.cmake_memcheck_linux:
    stage: build-test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest -VV -S .gitlab/ci/ctest_configure.cmake"
        - "$LAUNCHER ctest -VV -S .gitlab/ci/ctest_build.cmake"
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_memcheck.cmake"

    interruptible: true
    timeout: 5 minutes
